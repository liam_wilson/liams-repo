﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace exercise11
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click_Click(object sender, EventArgs e)
        {
            lblResult.Text = $"The total amounbt is {addGST(txtNumber1.Text, txtNumber2.Text)} including GST";
        }

        static double addGST(string number1, string number2)
        {
            var a = double.Parse(number1);
            var b = double.Parse(number2);
            var answer = 0.00;

            answer = (a + b) * 1.15;

            answer = System.Math.Round(answer, 2); //Currency has 2 decimal places

            return answer;
        }
    }
}
