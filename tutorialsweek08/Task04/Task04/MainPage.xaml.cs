﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task04
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            var colours = new List<string>();
            colours.Add("red");
            colours.Add("blue");
            colours.Add("green");
            colours.Add("yellow");

            ColourList.ItemsSource = colours;
        }

        private void ColourList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ColourList.SelectedIndex == 0)
            {
                Mid.Background = new SolidColorBrush(Colors.Red);
            }
            else if (ColourList.SelectedIndex == 1)
            {
                Mid.Background = new SolidColorBrush(Colors.Blue);
            }
            else if (ColourList.SelectedIndex == 2)
            {
                Mid.Background = new SolidColorBrush(Colors.Green);
            }
            else if (ColourList.SelectedIndex == 3)
            {
                Mid.Background = new SolidColorBrush(Colors.Yellow);
            }
            else
            {
                Mid.Background = new SolidColorBrush(Colors.Purple);
            }
        }
    }
}
