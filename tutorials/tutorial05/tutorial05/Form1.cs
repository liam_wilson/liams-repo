﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tutorial05
{
    public partial class Form1 : Form
    {
        List<string> item = new List<string>();
        public Form1()
        {
            InitializeComponent();
        }

        public List<string> items()
        {
            item.Add(textBox1.Text);
            return item;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.BeginUpdate();

            if(listBox1.Items.Count != 0)
            {
                listBox1.Items.Clear();
            }

            foreach (var item in items())
            {
                listBox1.Items.Add(item);
            }

            listBox1.EndUpdate();

            textBox1.Clear();
            textBox1.Focus();
        }
    }
}
