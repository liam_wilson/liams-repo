﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tutorial03
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            comboBox1.DataSource = fruits();
        }

        public static string[] fruits()
        {
            var fruit = new string[3] { "pear", "banana", "tomato", };

            return fruit;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            label1.Text = $"A {comboBox1.SelectedItem.ToString()} is a nice fruit!";
        }
    }
}
